<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Render view or JSON based on request.
     *
     * @return void
     */
    protected function makeView($request, $view, $data = [])
    {
        if ($request->wantsJson()) {
            return response()->json($data);
        }

        return view($view, $data);
    }

    /**
     * Render redirect or JSON based on request.
     *
     * @return void
     */
    protected function makeRedirect($request, $route = null, $data = [])
    {
        if ($request->wantsJson()) {
            return response()->json($data);
        }

        return redirect($route)->with($data);
    }
}
