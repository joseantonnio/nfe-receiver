<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Nfe;
use App\NfeValue;

class NfeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all NFes registered.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return parent::makeView($request, 'nfe.index', ['nfes' => NfeValue::paginate(10)]);
    }

    /**
     * Show create NFe form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        $nfe = new NfeValue();
        return parent::makeView($request, 'nfe.create', $nfe->getFillable());
    }

    /**
     * Find NFe value or request NFe XML to store the value.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'key' => 'required|string|size:44'
        ]);

        $receiver = new Nfe\Receiver($request->input('key'));
        $data = $receiver->hasNext();
        $nfe_value = NfeValue::update_value($data[0]->access_key, $data[0]->xml);

        return parent::makeView($request, 'nfe.create', [
            'nfe' => [
                'key' => $request->input('key'),
                'value' => $nfe_value->cents_value,
                'origin' => __($nfe_value->origin)
            ],
            'success' => __("Key Successfully Searched."),
        ]);
    }

    /**
     * Find all NFes and store their values.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function import(Request $request)
    {
        $receiver = new Nfe\Receiver();
        \DB::beginTransaction();

        try {
            while ($data = $receiver->hasNext()) {
                foreach ($data as $key => $nfe) {
                    NfeValue::update_value($nfe->access_key, $nfe->xml);
                }
            }

            \DB::commit();
            $success = __("Import completed successfully!");
        } catch (\Exception $e) {
            \DB::rollback();
            $error = __("Import aborted, no record saved! Here's some technical information:") . '<br /><code>' . strip_tags($e->getMessage()) . '</code>';
        }

        return parent::makeRedirect($request, '/nfe', [
            'status' => isset($success) ? $success : null,
            'error' => isset($error) ? $error : null,
            'nfes' => NfeValue::paginate(10),
        ]);
    }
}
