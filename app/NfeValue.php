<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Nfe;

class NfeValue extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'access_key', 'cents_value',
    ];

    /**
     * Create attribute to hold value origin information.
     *
     * @var array
     */
    public $origin;

    /**
	 * Override the Eloquent model instance.
	 *
	 * @param  array  $attributes
	 * @return void
	 */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->origin = __('Database');
    }

    /**
     * Static update method with XML file contents.
     *
     * @var string $access_key
     * @var string $xml
     *
     * @return integer
     */
    public static function update_value($access_key, $xml)
    {
        $nfe_value = (new static)::firstOrNew(['access_key' => $access_key]);
        $valuer = new Nfe\Valuer($xml);
        $value = $valuer->getValue();

        if ($nfe_value->cents_value != $value) {
            $nfe_value->origin = __("Arquivei API");
            $nfe_value->cents_value = $value;
            $nfe_value->save();
        }

        return $nfe_value;
    }
}
