<?php

namespace App\Nfe;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class Receiver
{
    /**
     * Arquivei received endpoint.
     *
     * @var string
     */
    private $url;

    /**
     * Guzzle client.
     *
     * @var GuzzleHttp\Client
     */
    private $client;

    /**
     * NFe key.
     *
     * @var string
     */
    private $accessKey;

    /**
     * Returned NFes.
     *
     * @var string
     */
    private $data;

    /**
     * Create a new NFe instance.
     *
     * @return void
     */
    public function __construct($accessKey = null)
    {
        $handlerStack = HandlerStack::create(new CurlHandler());
        $handlerStack->push(Middleware::retry($this->retryDecider(), $this->retryDelay()));
        $this->client = new Client(array('handler' => $handlerStack));

        $this->accessKey = $accessKey;
        $this->url = getenv("URL");
    }

    /**
     * Request NFe from Arquivei API.
     *
     * @return string
     */
    public function hasNext()
    {
        $response = $this->request();

        if (!empty($response->data)) {
            $this->data = $response->data;
            $this->url = $response->page->next;
            $this->decode();

            return $this->data;
        }

        return false;
    }

    /**
     * Decode NFes from Base64 to XML.
     *
     * @return string
     */
    public function decode()
    {
        foreach ($this->data as $key => $value) {
            $this->data[$key]->xml = base64_decode($value->xml);
        }

        return true;
    }

    /**
     * Create request and get NFe information.
     *
     * @return object
     */
    private function request()
    {
        $data = [
            'headers' => [
                'Content-Type' => 'application/json',
                'x-api-id' => getenv("X_API_ID"),
                'x-api-key' => getenv("X_API_KEY"),
            ],
        ];

        if (isset($this->accessKey)) {
            $data['query'] = [
                'access_key[0]' => $this->accessKey,
            ];
        }

        $response = $this->client->get($this->url, $data);
        return json_decode($response->getBody());
    }

    /**
     * Create a retry decider.
     *
     * @return object
     */
    private function retryDecider()
    {
        return function (
            $retries,
            Request $request,
            Response $response = null,
            RequestException $exception = null
        ) {
            if ($retries >= 5) {
                return false;
            }

            if ($exception instanceof ConnectException) {
                return true;
            }

            if ($response) {
                if ($response->getStatusCode() >= 300) {
                    return true;
                }
            }

            return false;
        };
    }

    /**
     * Add delays from 1 second to 5 seconds on retry.
     *
     * @return Closure
     */
    private function retryDelay()
    {
        return function ($numberOfRetries) {
            return 1000 * $numberOfRetries;
        };
    }
}