<?php

namespace App\Nfe;

class Valuer
{
    /**
     * NFe XML.
     *
     * @var SimpleXMLElement
     */
    private $xml;

    /**
     * Create a new Valuer instance.
     *
     * @return void
     */
    public function __construct($xml)
    {
        $this->xml = new \SimpleXMLElement($xml);
    }

    /**
     * Get value from NFe XML.
     *
     * @return integer
     */
    public function getValue()
    {
        if (empty($this->xml)) {
            throw new Exception("Empty XML.");
        }

        if (isset($this->xml->NFe->infNFe->total->ICMSTot->vNF)) {
            $value = (integer) (floatval($this->xml->NFe->infNFe->total->ICMSTot->vNF) * 100);
            return $value;
        }

        return;
    }
}