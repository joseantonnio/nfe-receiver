<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function()
    {
        Route::get('/', 'HomeController@index')->name('home');

        Route::get('/nfe', 'NfeController@index')->name('nfe.all');
        Route::get('/nfe/create', 'NfeController@create')->name('nfe.create');
        Route::post('/nfe', 'NfeController@store')->name('nfe.store');
        Route::get('/nfe/import', 'NfeController@import')->name('nfe.import');

        Auth::routes();
    }
);
