<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'middleware' => [ 'auth:api' ]
    ],
    function()
    {
        Route::get('/user', function (Request $request) {
            return $request->user();
        })->name('api.user');

        Route::get('/nfe', 'NfeController@index')->name('api.nfe.all');
        Route::get('/nfe/create', 'NfeController@create')->name('api.nfe.create');
        Route::post('/nfe', 'NfeController@store')->name('api.nfe.store');
        Route::get('/nfe/import', 'NfeController@import')->name('api.nfe.import');
    }
);
