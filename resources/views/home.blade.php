@extends('layouts.default')

@section('card-header')
    {{ __('Welcome to') }} <strong>{{ config('app.name', 'Laravel') }}</strong>
@endsection

@section('card-content')
    <div class="row">
        <div class="col-md-12">
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ route('nfe.create') }}">@lang("Find NFe value")</a></li>
                <li class="list-group-item"><a href="{{ route('nfe.all') }}">@lang("List stored NFes values")</a></li>
                <li class="list-group-item"><a href="{{ route('nfe.import') }}">@lang("Import all NFes values")</a> <span class="badge badge-pill badge-danger">@lang("New")</span></li>
            </ul>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12">
            <h3>@lang("API Access")</h3>
            <hr />
            <p>@lang('Your API Token is') <code>{{ Auth::user()->api_token }}</code>.</p>
            <p>@lang('This is a cURL access example:')<br />
            <code>
                curl -X GET \<br />
                http://localhost:8080/api/nfe \<br />
                -H 'Accept: application/json' \<br />
                -H 'Accept-Language: {{ Lang::locale() }}' \<br />
                -H 'Authorization: Bearer {{ Auth::user()->api_token }}' \<br />
                -H 'Content-Type: application/json'
            </code></p>
        </div>
    </div>
@endsection
