@extends('layouts.default')

@section('card-header')
    {{ __("NFes List") }}
@endsection

@section('card-content')
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">@lang("Access Key")</th>
                <th scope="col">@lang("Value")</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($nfes as $nfe)
                <tr>
                    <th scope="row">{{ $nfe["id"] }}</th>
                    <td>{{ $nfe["access_key"] }}</td>
                    <td>@money($nfe['cents_value'], __('USD'))</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    @if ($nfes->hasPages())
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                <li class="page-item {{ $nfes->previousPageUrl() ?: "disabled" }}">
                    <a class="page-link" href="{{ $nfes->previousPageUrl() ?: "#" }}">@lang("Previous")</a>
                </li>

                @for ($i = 1; $i <= $nfes->lastPage(); $i++)
                    <?php
                        $half = 3;
                        $from = $nfes->currentPage() - $half;
                        $to = $nfes->currentPage() + $half;

                        if ($nfes->currentPage() < $half) {
                            $to += $half - $nfes->currentPage();
                        }

                        if ($nfes->lastPage() - $nfes->currentPage() < $half) {
                            $from -= $half - ($nfes->lastPage() - $nfes->currentPage()) - 1;
                        }
                    ?>
                    @if ($from < $i && $i < $to)
                        <li class="page-item {{ ($nfes->currentPage() == $i) ? 'disabled' : '' }}">
                            <a class="page-link" href="{{ $nfes->url($i) }}">{{ $i }}</a>
                        </li>
                    @endif
                @endfor

                <li class="page-item {{ $nfes->nextPageUrl() ? "" : "disabled" }}">
                    <a class="page-link" href="{{ $nfes->nextPageUrl() ?: "#" }}">@lang("Next")</a>
                </li>
            </ul>
        </nav>
    @endif
@endsection
