@extends('layouts.default')

@section('card-header')
{{ __('Find NFe value') }}
@endsection

@section('card-content')

    @if (isset($success))
        <div class="alert alert-success" role="alert">
            {{ $success }}
        </div>
    @endif

    @if (isset($nfe))
        <div class="alert alert-info" role="alert">
            <strong>@lang("Access Key"):</strong> {{ $nfe['key'] }}<br />
            <strong>@lang("Value"):</strong> @money($nfe['value'], __('USD'))<br />
            <strong>@lang("Origin"):</strong> {{ $nfe['origin'] }}
        </div>
    @endif

    <form method="POST" action="{{ route('nfe.store') }}">
        @csrf

        <div class="form-group row">
            <label for="key" class="col-md-4 col-form-label text-md-right">{{ __('Access Key') }}</label>

            <div class="col-md-6">
                <input id="key" type="text" class="form-control @error('key') is-invalid @enderror" name="key" value="{{ old('key') }}" required autofocus>

                @error('key')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Submit') }}
                </button>
            </div>
        </div>
    </form>
@endsection