# NFe Receiver

With this application you can fetch NFes keys and values and store it. Choose between web application or API and use it as you want!

> This is my application test to Back End Engineer job vacancy at Arquivei.

## Table of Contents

[TOC]

## Versions

This application uses the following versions to run and some of them need to be locally installed.

|**Dependency**|**Version**|**Location**  |
|-------------	|---------- |-----------	|
| PHP           | 7.1       | nfe-app 	  |
| Composer      | 1.8       | nfe-app     |
| MySQL	        | 5.7 	    | nfe-mysql   |
| Ngix          | 1.12      | nfe-app     |
| Docker CE 	  | 18.06     | **local**   |
| Docker Compose| 1.22      | **local**   |
| Node.js       | 8.16      | nfe-node    |
| npm           | 6.4       | nfe-node    |

> You can quickly install the required applications following [Docker CE guide](https://docs.docker.com/install/) and [Docker Compose product manual](https://docs.docker.com/compose/install/).

## Getting started

You'll need [Docker CE](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/) in order to run this application.

### Installation

Clone this repository

`git clone git@bitbucket.org:joseantonnio/nfe-receiver.git`

Switch to the repository folder

`cd nfe-receiver`

Run the application

`docker-compose up -d`

Check application is running

`docker ps -a`

Copy the configuration example file

`docker exec -it nfe-app cp .env.example .env`

Install dependencies using composer

`docker exec -it nfe-app composer install`

Generate master key

`docker exec -it nfe-app php artisan key:generate`

Make migrations

`docker exec -it nfe-app php artisan migrate`

Done! You can now access the application at http://localhost:8080.

## Useful information

### Commands

Execute commands inside your Docker container

`docker exec -it [container] [command]`

### Containers

This are the containers available

- `nfe-app` - Application container with Laravel
- `nfe-mysql` - Database container with MySQL 5.7
- `nfe-node` - Development container with Node.js

### Environment variables

- `.env.example` file - Default preconfigured environment variables
- `.env` file - Any new environment variables should be set or modified in this file

> Development environment variables are already set in `.env.example` and you don't need to configure them to run this application.
> You just need to follow the *installation* steps.

### Commands aliases

With an alias you can easily execute the commands like the above.

You can create an alias with `alias nfeapp='docker exec -it nfe-app'` or `alias nfenode='docker-compose run --rm nfe-node'`.

After that, you just `nfeapp [command]` or `nfenode [command]` to run a command.

Here's some examples:

- `nfeapp php artisan migrate:status`
- `nfenode npm -v`

## Usage

### First steps

In a fresh start, you need to register with a valid e-mail address. Go to http://localhost:8080, choose `Create an account` on the right side of your screen. Then fill all the inputs with your data and press `Register`. Now, you have access to the application.

If it's not your first time at the application, probably you have an account. Just type your e-mail address and password and press `Login`. Check `Remember Me` if you want to keep your account logged in.

Don't remember your password? No problem. Use the `Forgot Your Password?` link and simply recover it.

### Basic usage

After logged in, you can go to `Find NFe values` and type or paste a NFe access key to retrieve it from Arquivei API or application database.

Every time you make a request, the value is checked with the value on Arquivei API.

You can access `List stored NFes values` too and see all stored NFe access keys and it's values.

### API

The application has 4 protected API endpoints:

- GET `/api/user` - Get current user information.
- GET `/api/nfe` - List all stored NFe with pagination.
    - Parameter `page` - Call a specific page.
- GET `/api/nfe/create` - List NFe fillable attributes.
- POST `/api/nfe` - Find or store a new NFe value to the database.
    - Parameter `key` - The NFe access key.
- GET `/api/nfe/import` - Import all NFes values to the database.

> Always send `Accept` header as `application/json`.
> Use `Accept-Language` to specify the content language.
> Always send your API token as [Bearer](https://swagger.io/docs/specification/authentication/bearer-authentication/) using `Authorization` header key.
> Always send POST data as `application/json` and add `Content-Type: application/json`.

This is a cURL API GET request example:
```
curl -X GET \
http://localhost:8080/api/nfe \
-H 'Accept: application/json' \
-H 'Accept-Language: en' \
-H 'Authorization: Bearer your-token' \
-H 'Content-Type: application/json'
```

And this is a cURL API POST request example:
```
  curl -X POST \
  http://localhost:8080/api/nfe \
  -H 'Accept: application/json' \
  -H 'Accept-Language: en' \
  -H 'Authorization: Bearer your-token' \
  -H 'Content-Type: application/json' \
  -d '{ "key": "35180209351027000101550010000017851000017854" }'
```

## License

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>

-----------------------------

Made with ❤ using Laravel.